import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

const kSendButtonTextStyle = TextStyle(
  color: Color(0xFFF89E1F),
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const kMessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Type your message here...',
  border: InputBorder.none,
);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Color(0xFF333333), width: 2.0),
  ),
);

Padding customButton(Color color, void onPressed(), String message) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 16.0),
    child: Material(
      elevation: 5.0,
      color: color,
      borderRadius: BorderRadius.circular(30.0),
      child: MaterialButton(
        onPressed: onPressed,
        minWidth: 200.0,
        height: 42.0,
        child: Text(
          message,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    ),
  );
}

AnimatedTextKit CustomAnimatedText(String text, double fontsize) {
  return AnimatedTextKit(totalRepeatCount: 1, animatedTexts: [
    TypewriterAnimatedText(
      text,
      speed: const Duration(milliseconds: 250),
      textStyle: TextStyle(
        color: Color(0xFF444444),
        fontWeight: FontWeight.w500,
        fontSize: fontsize,
      ),
    ),
  ]);
}

const kTextFieldDecoration = InputDecoration(
  hintText: '',
  hintStyle: TextStyle(color: Colors.grey),
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF444444), width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF444444), width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
);
